package io.github.CodedNil;

import java.util.List;

import org.bukkit.DyeColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Animals;
import org.bukkit.entity.Chicken;
import org.bukkit.entity.Cow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Horse;
import org.bukkit.entity.MushroomCow;
import org.bukkit.entity.Ocelot;
import org.bukkit.entity.Pig;
import org.bukkit.entity.Player;
import org.bukkit.entity.Sheep;
import org.bukkit.entity.Tameable;
import org.bukkit.entity.Villager;
import org.bukkit.entity.Villager.Profession;
import org.bukkit.entity.Wolf;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class AnimalTag {
	public static void Release(ItemStack item, Block block, Player player) {
		ItemMeta meta = item.getItemMeta();
		List<String> lore = meta.getLore();
		String type = lore.get(0).split(": ")[1].toUpperCase();
		Location location = block.getLocation().add(0, 2, 0);
		if (type.equals("VILLAGER")) {
			Villager animal = player.getWorld().spawn(location, Villager.class);
			if (!lore.get(1).substring(6).equals("null"))
				animal.setCustomName(lore.get(1).substring(6));
			animal.setAge(Integer.parseInt(lore.get(2).split(": ")[1]));
			animal.setHealth(Double.parseDouble(lore.get(3).split(": ")[1]));
			animal.setMaxHealth(Double.parseDouble(lore.get(4).split(": ")[1]));
			Util.setEntitySpeed(animal,
					Double.parseDouble(lore.get(5).split(": ")[1]));
			animal.setBreed(Boolean.parseBoolean(lore.get(6).split(": ")[1]));
			animal.setProfession(Profession.valueOf(lore.get(7).split(": ")[1]));
		} else {
			Animals animal = null;
			if (type.equals("HORSE")) {
				animal = player.getWorld().spawn(location, Horse.class);
			} else if (type.equals("WOLF")) {
				animal = player.getWorld().spawn(location, Wolf.class);
			} else if (type.equals("OCELOT")) {
				animal = player.getWorld().spawn(location, Ocelot.class);
			} else if (type.equals("SHEEP")) {
				animal = player.getWorld().spawn(location, Sheep.class);
			} else if (type.equals("PIG")) {
				animal = player.getWorld().spawn(location, Pig.class);
			} else if (type.equals("COW")) {
				animal = player.getWorld().spawn(location, Cow.class);
			} else if (type.equals("CHICKEN")) {
				animal = player.getWorld().spawn(location, Chicken.class);
			} else if (type.equals("MUSHROOM_COW")) {
				animal = player.getWorld().spawn(location, MushroomCow.class);
			}
			if (animal == null)
				return;
			if (animal instanceof Tameable)
				((Tameable) animal).setOwner(player);
			if (!lore.get(1).substring(6).equals("null"))
				animal.setCustomName(lore.get(1).substring(6));
			animal.setAge(Integer.parseInt(lore.get(2).split(": ")[1]));
			animal.setHealth(Double.parseDouble(lore.get(3).split(": ")[1]));
			animal.setMaxHealth(Double.parseDouble(lore.get(4).split(": ")[1]));
			Util.setEntitySpeed(animal,
					Double.parseDouble(lore.get(5).split(": ")[1]));
			animal.setBreed(Boolean.parseBoolean(lore.get(6).split(": ")[1]));
			if (animal instanceof Horse) {
				((Horse) animal).setJumpStrength(Double.parseDouble(lore.get(7)
						.split(": ")[1]));
				((Horse) animal).setCarryingChest(Boolean.parseBoolean(lore
						.get(8).split(": ")[1]));
				((Horse) animal).setVariant(Horse.Variant.valueOf(lore.get(9)
						.split(": ")[1]));
				((Horse) animal).setStyle(Horse.Style.valueOf(lore.get(10)
						.split(": ")[1]));
				((Horse) animal).setColor(Horse.Color.valueOf(lore.get(11)
						.split(": ")[1]));
			} else if (animal instanceof Wolf) {
				((Wolf) animal).setCollarColor(DyeColor.valueOf(lore.get(7)
						.split(": ")[1]));
			} else if (animal instanceof Ocelot) {
				((Ocelot) animal).setCatType(Ocelot.Type.valueOf(lore.get(7)
						.split(": ")[1]));
			} else if (animal instanceof Sheep) {
				((Sheep) animal).setColor(DyeColor.valueOf(lore.get(7).split(
						": ")[1]));
				((Sheep) animal).setSheared(Boolean.parseBoolean(lore.get(8)
						.split(": ")[1]));
			}
		}
		Util.removeOneOf(player);
		if (player.getGameMode() != GameMode.CREATIVE) {
			player.getWorld().playSound(player.getLocation(), Sound.ITEM_BREAK,
					1, 1);
		}
	}

	public static void Capture(Entity target, Player player) {
		ItemMeta meta = player.getItemInHand().getItemMeta();
		meta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, false);
		List<String> lore = Util.getStats(target);
		if (target instanceof Horse)
			Util.dropInventory(((Horse) target).getInventory(), target);
		meta.setLore(lore);
		meta.setDisplayName("AnimalTag");
		target.remove();
		player.getItemInHand().setItemMeta(meta);
		player.getWorld().playSound(player.getLocation(), Sound.ITEM_PICKUP, 1,
				1);
	}

	public static void Read(Entity target, Player player) {
		List<String> lore = Util.getStats(target);
		for (int i = 0; i < lore.size(); i++) {
			player.sendMessage(lore.get(i));
		}
	}

	public static void Transform(Entity target, Player player,
			ItemStack itemHand) {
		Util.removeOneOf(player);
		if (player.getGameMode() != GameMode.CREATIVE) {
			if (Math.round(Math.random() * 200) == 0) {
				if (itemHand.isSimilar(new ItemStack(Material.ROTTEN_FLESH))) {
					((Horse) target).setVariant(Horse.Variant.UNDEAD_HORSE);
				} else if (itemHand.isSimilar(new ItemStack(Material.BONE))) {
					((Horse) target).setVariant(Horse.Variant.SKELETON_HORSE);
				}
			}
		} else {
			if (itemHand.isSimilar(new ItemStack(Material.ROTTEN_FLESH))) {
				((Horse) target).setVariant(Horse.Variant.UNDEAD_HORSE);
			} else if (itemHand.isSimilar(new ItemStack(Material.BONE))) {
				((Horse) target).setVariant(Horse.Variant.SKELETON_HORSE);
			}
		}
	}

	public static boolean isAnimalTag(ItemStack item) {
		if (item == null)
			return false;
		if (!item.hasItemMeta())
			return false;
		if (!item.getItemMeta().hasDisplayName())
			return false;
		if (!item.containsEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL))
			return false;
		if (item.getType() != Material.NAME_TAG)
			return false;
		if (!item.getItemMeta().getDisplayName().equalsIgnoreCase("Animal Tag"))
			return false;
		return true;
	}
}
