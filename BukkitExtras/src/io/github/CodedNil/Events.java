package io.github.CodedNil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Animals;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Pig;
import org.bukkit.entity.Player;
import org.bukkit.entity.Sheep;
import org.bukkit.entity.Tameable;
import org.bukkit.entity.Villager;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.event.player.PlayerShearEntityEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;

@SuppressWarnings("deprecation")
public class Events {
	public static void PlayerTeleport(PlayerTeleportEvent event) {
		Location to = event.getTo();
		Location from = event.getFrom();
		Player player = event.getPlayer();
		if (event.getCause() == TeleportCause.ENDER_PEARL) {
			if (Compat.playerCanAccess(player, to)
					&& Compat.playerCanAccess(player, from))
				return;
			event.setCancelled(true);
			Util.giveItem(player, new ItemStack(Material.ENDER_PEARL));
		}
	}

	public static void PlayerPortal(PlayerPortalEvent event) {
		Location to = event.getTo();
		Location from = event.getFrom();
		Player player = event.getPlayer();
		if (event.getCause() == TeleportCause.NETHER_PORTAL) {
			if (Compat.playerCanAccess(player, to)
					&& Compat.playerCanAccess(player, from))
				return;
			event.setCancelled(true);
		}
	}

	public static void CreatureSpawn(CreatureSpawnEvent event) {
		LivingEntity creature = event.getEntity();
		if (event.getSpawnReason() == SpawnReason.BREEDING)
			if (creature.getType() == EntityType.PIG)
				for (int i = 0; i < Math.random() * 3 + 1; i++) {
					Pig pig = (Pig) creature;
					Pig animal = creature.getWorld().spawn(
							creature.getLocation(), Pig.class);
					animal.setAge(pig.getAge());
				}
	}

	public static void ShearEntity(PlayerShearEntityEvent event) {
		Entity entity = event.getEntity();
		if (entity instanceof Sheep)
			for (int i = 0; i < Math.random() * 3; i++)
				entity.getWorld().dropItemNaturally(entity.getLocation(),
						new ItemStack(Material.STRING));
	}

	public static void BlockBreak(BlockBreakEvent event) {
		Block block = event.getBlock();
		Player player = event.getPlayer();
		Material blocktype = block.getType();
		ItemStack item = player.getItemInHand();
		if (Util.isA(blocktype, "Crop")) {
			BetterHoe.breakCrop(block, player, blocktype, item, event);
			return;
		}
		if (event.isCancelled())
			return;
		if (blocktype == Material.MOB_SPAWNER
				&& item.containsEnchantment(Enchantment.SILK_TOUCH)
				&& Util.isA(item.getType(), "Pickaxe")) {
			event.setExpToDrop(0);
			CreatureSpawner state = (CreatureSpawner) block.getState();
			ItemStack spawner = new ItemStack(Material.MOB_SPAWNER);
			ItemMeta itemMeta = spawner.getItemMeta();
			List<String> lore = new ArrayList<String>();
			lore.add(state.getCreatureTypeName());
			itemMeta.setLore(lore);
			spawner.setItemMeta(itemMeta);
			player.getWorld().dropItemNaturally(block.getLocation(), spawner);
		}
		if (block.getDrops().size() != 1)
			return;
		String drops = block.getDrops().toString();
		Material drop = Material
				.valueOf(drops.substring(11, drops.length() - 6));
		if (event.isCancelled())
			return;
		if (player.getGameMode() == GameMode.CREATIVE)
			return;
		if (EnchantmentHandler.hasCustomEnchant("Fiery", item)) {
			if (Util.getSmeltedForm(drop) == null)
				return;
			if (!player.hasPermission("bukkitextras.customenchants.fiery"))
				return;
			event.setCancelled(true);
			block.setType(Material.AIR);
			Material blockDrop = Util.getSmeltedForm(drop);
			int fortunelevel = item
					.getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS);
			if (blockDrop.isBlock() || fortunelevel == 0) {
				player.getWorld().dropItemNaturally(block.getLocation(),
						new ItemStack(blockDrop));
			} else {
				for (int i = 0; i < Util.multiplyByFortune(fortunelevel); i++) {
					player.getWorld().dropItemNaturally(block.getLocation(),
							new ItemStack(blockDrop));
				}
			}
			Util.damageItem(player, item, player.getInventory()
					.getHeldItemSlot());
		} else if (EnchantmentHandler.hasCustomEnchant("Feller", item)) {
			if (blocktype != Material.LOG && blocktype != Material.LOG_2)
				return;
			if (player.isSneaking())
				return;
			if (!player.hasPermission("bukkitextras.customenchants.feller"))
				return;
			event.setCancelled(true);
			int level = EnchantmentHandler
					.getCustomEnchantLevel("Feller", item);
			int high;
			if (blocktype == Material.LOG)
				high = Util.getAllOfBlock(block.getLocation(), Material.LOG, 1,
						level * 2);
			else
				high = Util.getAllOfBlock(block.getLocation(), Material.LOG_2,
						1, level * 2);
			for (int x = 0; x < 8; x++) {
				Location loc = block.getLocation();
				if (level >= 3)
					Util.addLoc(loc, x);
				for (int i = 0; i < high + 1; i++) {
					Location currLoc = Util.changeLoc(loc, new Vector(0, i, 0));
					Block currBlock = currLoc.getBlock();
					if (currBlock.getType() == Material.LOG
							|| currBlock.getType() == Material.LOG_2) {
						currBlock.breakNaturally(item);
						Util.damageItem(player, item, player.getInventory()
								.getHeldItemSlot());
					}
				}
				if (level < 3)
					return;
			}
		}
	}

	public static void BlockPlace(BlockPlaceEvent event) {
		Block block = event.getBlock();
		Material blocktype = block.getType();
		ItemStack item = event.getItemInHand();
		if (!item.hasItemMeta())
			return;
		if (!item.getItemMeta().hasLore())
			return;
		if (blocktype == Material.MOB_SPAWNER) {
			CreatureSpawner state = (CreatureSpawner) block.getState();
			state.setCreatureTypeByName(item.getItemMeta().getLore().get(0));
		}
	}

	public static void PlayerSneak(PlayerToggleSneakEvent event) {
		Player player = event.getPlayer();
		if (event.isSneaking()) {
			if (!event.getPlayer().hasPermission("bukkitextras.elevators"))
				return;
			Block block = player.getLocation().getBlock()
					.getRelative(BlockFace.DOWN);
			if (block.getType() == BukkitExtras.Plugin.elevatorBlock) {
				if (!Compat.playerCanAccess(player, block.getLocation()))
					return;
				Material elevatorBlock = BukkitExtras.Plugin.elevatorBlock;
				Block target = Util.findNearest(block.getLocation(),
						elevatorBlock, -1, true);
				if (target != null) {
					Location playerLoc = player.getLocation();
					float yaw = playerLoc.getYaw();
					float pitch = playerLoc.getPitch();
					Location location = target.getLocation().add(
							new Vector(0.5, 1, 0.5));
					location.setYaw(yaw);
					location.setPitch(pitch);
					player.teleport(location);
					player.getWorld().playSound(target.getLocation(),
							Sound.ENDERMAN_TELEPORT, (float) 0.5, 1);
					player.getWorld().playEffect(
							target.getLocation().add(0, 1, 0),
							Effect.ENDER_SIGNAL, 1);
					player.getWorld().playEffect(
							block.getLocation().add(0, 1, 0),
							Effect.ENDER_SIGNAL, 1);
				}
			}
		}
	}

	public static void PlayerMove(PlayerMoveEvent event) {
		Player player = event.getPlayer();
		Location movement = event.getTo().clone().subtract(event.getFrom());
		if (movement.getY() > 0.4) {
			Block block = player.getLocation().getBlock()
					.getRelative(BlockFace.DOWN);
			if (block.getType() == BukkitExtras.Plugin.elevatorBlock) {
				if (!event.getPlayer().hasPermission("bukkitextras.elevators"))
					return;
				if (!Compat.playerCanAccess(player, block.getLocation()))
					return;
				Material elevatorBlock = BukkitExtras.Plugin.elevatorBlock;
				Block target = Util.findNearest(block.getLocation(),
						elevatorBlock, 1, true);
				if (target != null) {
					Location playerLoc = player.getLocation();
					float yaw = playerLoc.getYaw();
					float pitch = playerLoc.getPitch();
					Location location = target.getLocation().add(
							new Vector(0.5, 1, 0.5));
					location.setYaw(yaw);
					location.setPitch(pitch);
					player.teleport(location);
					player.getWorld().playSound(target.getLocation(),
							Sound.ENDERMAN_TELEPORT, (float) 0.5, 1);
					player.getWorld().playEffect(
							target.getLocation().add(0, 1, 0),
							Effect.ENDER_SIGNAL, 1);
					player.getWorld().playEffect(
							block.getLocation().add(0, 1, 0),
							Effect.ENDER_SIGNAL, 1);
				}
			}
		}
	}

	public static void PlayerInteract(PlayerInteractEvent event) {
		Block block = event.getClickedBlock();
		Player player = event.getPlayer();
		ItemStack item = event.getItem();
		if (event.getAction().name().contains("RIGHT") && item != null) {
			if (WandHandler.isWand(item)) {
				if (!player.hasPermission("bukkitextras.wands"))
					return;
				if (WandHandler.isWandOf(item, "Crafting"))
					player.openWorkbench(player.getLocation(), true);
				else if (WandHandler.isWandOf(item, "Ender"))
					player.openInventory(player.getEnderChest());
				else if (WandHandler.isWandOf(item, "Enchanting"))
					player.openEnchanting(player.getLocation(), true);
			}
		}
		if (block == null)
			return;
		Material blocktype = block.getType();
		if (event.getAction() == Action.LEFT_CLICK_BLOCK)
			if (Util.isA(event.getClickedBlock().getType(), "Crop")) {
				event.setCancelled(false);
				return;
			}
		if (event.getAction() != Action.RIGHT_CLICK_BLOCK)
			return;
		if (event.isCancelled())
			if (blocktype == Material.BREWING_STAND)
				event.setCancelled(!Compat.playerCanBrew(player,
						block.getLocation()));
			else if (blocktype == Material.CHEST)
				if (Util.hasAdjacent(block, Material.BREWING_STAND, true))
					event.setCancelled(!Compat.playerCanBrew(player,
							block.getLocation()));
		if (event.getItem() == null)
			return;
		if (Util.isA(player.getItemInHand().getType(), "Hoe")) {
			if (!event.getPlayer().hasPermission("bukkitextras.betterhoes"))
				return;
			if (Util.isA(blocktype, "GrowCrop"))
				BetterHoe.growCrop(block, player);
			else if (blocktype == Material.SOIL)
				BetterHoe.plantCrop(block, player);
		}
		if (event.isCancelled())
			return;
		if (WandHandler.isWandOf(item, "Trading")) {
			if (!player.hasPermission("bukkitextras.wands"))
				return;
			if (Compat.playerCanBuild(player, block.getLocation()))
				if (player.isSneaking()) {
					ItemMeta itemMeta = item.getItemMeta();
					List<String> lore = new ArrayList<String>();
					lore.add(block.getType().toString());
					lore.add("" + block.getData());
					itemMeta.setLore(lore);
					item.setItemMeta(itemMeta);
				} else {
					ItemMeta itemMeta = item.getItemMeta();
					List<String> lore = itemMeta.getLore();
					Inventory inventory = player.getInventory();
					if (itemMeta.hasLore()) {
						Material material = Material.valueOf(lore.get(0));
						byte data = Byte.parseByte(lore.get(1));
						ItemStack itemstack = new ItemStack(material);
						itemstack.setDurability(data);
						if ((block.getType() == material)
								&& (block.getData() == data))
							return;
						if (player.getGameMode() == GameMode.CREATIVE) {
							block.setType(material);
							block.setData(data);
							player.getWorld().playSound(block.getLocation(),
									Sound.DIG_STONE, (float) 0.5, 1);
						} else if (inventory.containsAtLeast(itemstack, 1)) {
							Collection<ItemStack> drops = block
									.getDrops(new ItemStack(
											Material.IRON_PICKAXE));
							block.setType(material);
							block.setData(data);
							Util.removeItem(player, itemstack, 1);
							player.getWorld().playSound(block.getLocation(),
									Sound.DIG_STONE, (float) 0.5, 1);
							for (ItemStack itemDrop : drops)
								Util.giveItem(player, itemDrop);
						}
						player.updateInventory();
					}
				}
		}
		if (AnimalTag.isAnimalTag(item) && player.isSneaking()) {
			if (!player.hasPermission("bukkitextras.nametaganimals"))
				return;
			AnimalTag.Release(item, event.getClickedBlock(), player);
		}
	}

	public static void PlayerInteractEntity(PlayerInteractEntityEvent event) {
		if (event.getPlayer() == null)
			return;
		Player player = event.getPlayer();
		Entity target = event.getRightClicked();
		ItemStack itemHand = player.getItemInHand();
		if (AnimalTag.isAnimalTag(itemHand)) {
			event.setCancelled(true);
			return;
		}
		if (player.isSneaking()) {
			if (player.getItemInHand() == null)
				return;
			if (target instanceof Animals || target instanceof Villager)
				if (itemHand.equals(new ItemStack(Material.NAME_TAG))) {
					if (!player.hasPermission("bukkitextras.animaltags"))
						return;
					if (event.isCancelled())
						return;
					if (target instanceof Tameable
							&& ((Tameable) target).getOwner() != player)
						return;
					event.setCancelled(true);
					AnimalTag.Capture(target, player);
				} else if (itemHand.isSimilar(new ItemStack(Material.BOOK))) {
					if (!player.hasPermission("bukkitextras.readanimals"))
						return;
					event.setCancelled(true);
					AnimalTag.Read(target, player);
				}
			if (event.isCancelled())
				return;
			if (target instanceof Horse) {
				if (((Horse) target).getVariant() == Horse.Variant.UNDEAD_HORSE
						|| ((Horse) target).getVariant() == Horse.Variant.SKELETON_HORSE)
					return;
				if (!player.hasPermission("bukkitextras.specialhorses"))
					return;
				if (!itemHand.isSimilar(new ItemStack(Material.ROTTEN_FLESH))
						&& !itemHand.isSimilar(new ItemStack(Material.BONE)))
					return;
				event.setCancelled(true);
				AnimalTag.Transform(target, player, itemHand);
			}
		}
		if (target instanceof Villager)
			event.setCancelled(!Compat.playerCanTrade(player,
					target.getLocation()));
	}

	public static boolean onCommand(CommandSender sender, Command cmd,
			String[] args) {
		if (cmd.getName().equalsIgnoreCase("givewand")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				if (args.length != 1) {
					player.sendMessage("Invalid number of arguments");
					return false;
				} else {
					for (int i = 0; i < WandHandler.wands.size(); i++) {
						ItemStack wand = WandHandler.wands.get(i);
						String wandName = wand.getItemMeta().getDisplayName()
								.replaceAll("Wand of ", "");
						if (args[0].equalsIgnoreCase(wandName)) {
							Util.giveItem(player, wand);
							return true;
						}
					}
					String message = "Wand not found, valid wands are: ";
					for (int i = 0; i < WandHandler.wands.size(); i++) {
						ItemStack wand = WandHandler.wands.get(i);
						String wandName = wand.getItemMeta().getDisplayName()
								.replaceAll("Wand of ", "");
						message = message.concat("�6" + wandName);
						if (i != WandHandler.wands.size() - 1)
							message = message.concat(", ");
					}
					player.sendMessage(message);
				}
				return true;
			}
		} else if (cmd.getName().equalsIgnoreCase("enchantcustom")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				ItemStack handTool = player.getItemInHand();
				if (args.length != 2) {
					player.sendMessage("Invalid number of arguments");
					return false;
				} else {
					if (handTool.getType() == Material.AIR) {
						player.sendMessage("Cannot enchant air");
						return true;
					}
					if (!EnchantmentHandler.isEnchantValid(args[0])) {
						String message = "Enchantment not found, valid enchants are: ";
						for (int i = 0; i < EnchantmentHandler.enchants.size(); i++) {
							List<?> enchant = EnchantmentHandler.enchants
									.get(i);
							String enchantname = enchant.get(0).toString();
							player.sendMessage(enchantname);
							message = message.concat("�6" + enchantname);
							if (i != EnchantmentHandler.enchants.size() - 1)
								message = message.concat(", ");
						}
						player.sendMessage(message);
						return true;
					}
					if (!Util.isInteger(args[1])) {
						player.sendMessage("Level must be a integer");
						return true;
					}
					EnchantmentHandler.RemoveEnchant(handTool, args[0]);
					int level = Integer.parseInt(args[1]);
					if (level != 0)
						EnchantmentHandler
								.EnchantItem(handTool, args[0], level);
				}
				return true;
			}
		}
		return false;
	}
}